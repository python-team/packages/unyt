Source: unyt
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               pybuild-plugin-pyproject,
               python3-all,
               python3-numpy,
               python3-packaging,
               python3-pytest,
               python3-setuptools,
               python3-setuptools-scm,
               python3-sympy
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/unyt
Vcs-Git: https://salsa.debian.org/python-team/packages/unyt.git
Homepage: https://github.com/yt-project/unyt
Rules-Requires-Root: no

Package: python3-unyt
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Python package for handling numpy arrays with units
 Often writing code that deals with data that has units can be
 confusing. A function might return an array but at least with plain
 NumPy arrays, there is no way to easily tell what the units of the
 data are without somehow knowing a priori.
 .
 The unyt package (pronounced like “unit”) provides a subclass of
 NumPy’s ndarray class that knows about units.
